# Workflows for Alfred
* Apple Color Lists
This workflow gives quick access to the colors defined in Apple's color lists via "clr [color]". Colors can be copied to clipboard in hexadecimal, integer (press [cmd]) and floating point format (press [alt]), respectively.
* LaTexIt
Use [LaTexIt] (http://pierre.chachatelier.fr/latexit/) to quickly compile LaTex sourcecode from Alfred. Enter "$ [latexcode]" followed by your latex code and press [ENTER].
* Network Location
Quickly change network location using "loc [profile]" from Alfred
* VPN Toggle
Shows a list of all available VPN services configured in Shimo (http://www.chungwasoft.com/) and allows to connect or disconnect via Alfred. Usage "vpn [profile]".