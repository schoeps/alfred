//
//  main.m
//  colorread
//
//  Created by Sebastian Schöps on 05.11.17.
//  Copyright © 2017 Sebastian Schöps. All rights reserved.
//  clang main.m -fobjc-arc -fmodules -mmacosx-version-min=10.6 -o colorread

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *aColorLists = [NSColorList availableColorLists];
        NSColorList *cl;
        NSArray *aColorKeys;
        NSColor *myColor;
        NSString *myHex;
        NSInteger i;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *fileString;
        NSString *fileContent;
        NSError *error;
        
        int redIntValue, greenIntValue, blueIntValue;
        NSString *redHexValue, *greenHexValue, *blueHexValue;
        double redFloatValue, greenFloatValue, blueFloatValue;
        
        printf("{\"items\": [\n\n");
        i=0;
        for (id myList in aColorLists) {
            //NSLog(@"Listname = %@", [myList name]);
            cl = [NSColorList colorListNamed:[myList name]];
            aColorKeys = [cl allKeys];
            for (id myColorKey in aColorKeys) {
                myColor = [cl colorWithKey:myColorKey];
                
                // Get the red, green, and blue components of the color
                // https://developer.apple.com/library/content/qa/qa1576/_index.html
                NSColor *convertedColor=[myColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
                [convertedColor getRed:&redFloatValue green:&greenFloatValue blue:&blueFloatValue alpha:NULL];
                redIntValue=redFloatValue*255.99999f;
                greenIntValue=greenFloatValue*255.99999f;
                blueIntValue=blueFloatValue*255.99999f;
                
                // Convert the numbers to hex strings
                redHexValue=[NSString stringWithFormat:@"%02x", redIntValue];
                greenHexValue=[NSString stringWithFormat:@"%02x", greenIntValue];
                blueHexValue=[NSString stringWithFormat:@"%02x", blueIntValue];
                
                // Concatenate the red, green, and blue components' hex strings together with a "#"
                myHex = [NSString stringWithFormat:@"%@%@%@", redHexValue, greenHexValue, blueHexValue];
                
                // try to create file
                fileString = [myHex stringByAppendingString:@".ppm"];
                
                if (myHex != nil) {
                    if (i>0) {printf(",\n\n");}
                    printf("  {\n");
                    printf("    \"title\": \"%s (%s)\",\n",[myColorKey UTF8String],[[myList name] UTF8String]);
                    printf("    \"match\": \"%s %s\",\n",[myColorKey UTF8String],[[myList name] UTF8String]);
                    printf("    \"uid\": \"%s\",\n",[myColorKey UTF8String]);
                    printf("    \"subtitle\": \"Hex: #%s\",\n",[myHex UTF8String]);
                    printf("    \"arg\": \"%s\",\n",[myHex UTF8String]);
                    printf("    \"icon\": {\n");
                    printf("      \"path\": \"%s.ppm\"\n",[myHex UTF8String]);
                    printf("    },\n");
                    printf("    \"mods\": {\n");
                    printf("      \"cmd\": {\n");
                    printf("        \"subtitle\": \"Int: %d %d %d\",\n",redIntValue, greenIntValue, blueIntValue);
                    printf("        \"arg\": \"%d %d %d\"\n",redIntValue, greenIntValue, blueIntValue);
                    printf("      },\n");
                    printf("      \"alt\": {\n");
                    printf("        \"subtitle\": \"Float: %f %f %f\",\n",redFloatValue, greenFloatValue, blueFloatValue);
                    printf("        \"arg\": \"%f %f %f\"\n",redFloatValue, greenFloatValue, blueFloatValue);
                    printf("      }\n");
                    printf("    },\n");
                    printf("    \"variables\": {\n");
                    printf("      \"myColorKey\": \"%s\",\n",[myColorKey UTF8String]);
                    printf("      \"myListName\": \"%s\"\n",[[myList name] UTF8String]);
                    printf("    }\n");
                    printf("  }");
                    
                    if (![fileManager fileExistsAtPath:fileString]){
                        fileContent = [NSString stringWithFormat:@"P3 1 1 255\n%d %d %d\n", redIntValue,greenIntValue,blueIntValue];
                        if (![fileContent writeToFile:fileString atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
                            NSLog(@"Error: %@", [error userInfo]);
                        }
                    }
                }
                i=i+1;
            }
        }
        printf("\n\n]}\n");

    }
    return 0;
}



